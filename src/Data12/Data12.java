package Data12;

import javax.xml.crypto.Data;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class Data12 {

    public static void main(String[] args) {

    //    LocalDateTime data1 = LocalDateTime.of(1921, 07, 25, 00,00,00);
    //    DateTimeFormatter format = DateTimeFormatter.ofPattern("MM.dd.yyyy");
        String landing2 = "25.07.1921";
        String landing1 = "25.07.2012";

        DateTimeFormatter format  =  DateTimeFormatter.ofPattern("dd.MM.yyyy");
        LocalDate data1 = LocalDate.parse(landing1, format);
        LocalDate data2 = LocalDate.parse(landing2, format);

        if (data1.isAfter(data2)) {

            System.out.println(ChronoUnit.DAYS.between(data2,data1));

        }
        else{

            System.out.println(ChronoUnit.DAYS.between(data1,data2));
        }
        //ChronoUnit.DAYS.between(data1,data2);
        //LocalDate data2 = LocalDate.parse("25-07-1921");
       // System.out.println(ChronoUnit.DAYS.between(data2,data1));

    }
}
