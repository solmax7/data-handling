package Number21;

import java.math.BigDecimal;

public class Number21 {

    public static void main(String[] args) {

        BigDecimal pi = new BigDecimal(Math.PI);

        BigDecimal radius = new BigDecimal(2);

      BigDecimal  ploshad = pi.multiply(radius.pow(2));

        System.out.println(ploshad.setScale(50));
    }

}
