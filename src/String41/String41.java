package String41;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class String41 {

    public static void main(String[] args) {

        String input = "Ребе, Ви случайно не знаете, сколько тогда Иуда получил по нынешнему курсу?";
        Pattern p = Pattern.compile("(^[А-Я]|[а-я]|[А-Я])+");
        Matcher m = p.matcher(input);

        while ( m.find() ) {
            System.out.println(input.substring(m.start(), m.end()).toLowerCase());
        }

    }
}
