package Data13;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.Locale;

public class Data13 {

    public static void main(String[] args) {

        String d = "Friday, Aug 12, 2016 12:10:56 PM";

        DateTimeFormatter format  =  DateTimeFormatter.ofPattern("EEEE, MMM dd, yyyy HH:mm:ss a", Locale.US);

        LocalDateTime result = LocalDateTime.parse(d, format);

        DateTimeFormatter format1  =  DateTimeFormatter.ofPattern("yyyy MM dd", Locale.US);

        System.out.println(result.format(format1));

    }

}
