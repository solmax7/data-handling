package zadacha1;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Locale;
import java.util.PrimitiveIterator;

public class Data1 {

    public static void main(String[] args) {

        LocalDateTime dateBithday = LocalDateTime.of(1982, 06, 02, 12,32,5);

        LocalDateTime dayToday = LocalDateTime.now();

        long second =  ChronoUnit.SECONDS.between(dateBithday,dayToday);
        long minute =  ChronoUnit.MINUTES.between(dateBithday,dayToday);
        long hours =  ChronoUnit.HOURS.between(dateBithday,dayToday);
        long day =  ChronoUnit.DAYS.between(dateBithday,dayToday);
        long months =  ChronoUnit.MONTHS.between(dateBithday,dayToday);
        long year =  ChronoUnit.YEARS.between(dateBithday,dayToday);

        System.out.println("Дата рождения: " + dateBithday);
        System.out.println("Текущая дата: " + dayToday);
        System.out.println("Возраст в секундах: " + second);
        System.out.println("Возраст в минутах: " + minute);
        System.out.println("Возраст в часах: " + hours);
        System.out.println("Возраст в днях: " + day);
        System.out.println("Возраст в месяцах: " + months);
        System.out.println("Возраст в годах: " + year);

    }


}
